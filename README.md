<h1>Weather Forecast Android App</h1>

This is a very simple weather forecast information provider android application which consumes RESTful Web service from forecast.io

The app has 4 main sections defined as given below,
 <br>●   Current<br>
     This will show current weather data state and the icon will change depending on the icon value sent from the web    service.
 <br>●   Hourly<br>
     Displays a list of weather data values in an hourly basis and the corresponding icon will change according to the icon data sent from the 
     web service.
 <br>●   Daily<br>
     Displays a list of weather data values in a daily basis. data categorized in 4 sections in the list item as Sun and moon, precipitation, 
     temperature, apparent temperature, misc details.
 <br>●   Alerts<br>
     This section will show important weather alerts given by the Web service. Data might not be available at all times if so it will show as "No Alerts!"
     (to get some data in this section the GPS location can be given manually at application invocation point)
   
Weather data are presented with corresponding to the device current location which is taken by Android LocationManager.

An intent service(DataService) is used here to handle network invocations as it will not interrupt the UI thread due to heavy operations are done in a different
background thread.

On successful network invocation the returned JSON will be extracted and mapped to custom data structures as defined in the project.

A broadcast receiver is used to inform the UI after the intent service job is completed and the adapters etc. in the UI will update accordingly.
