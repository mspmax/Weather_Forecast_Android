package com.panduka.weatherforecast.datastructure;

/**
 * Created by Panduka on 2/13/2016.
 */
public class Flags {
    public String[] sources;
    public String[] isd_stations;
    public String[] madis_stations;
    public String[] darksky_stations;
    public String[] datapoint_stations;
    public String[] lamp_stations;
    public String[] metar_stations;
    public String units;
    public String darksky_unavailable;
    public String metno_license;

}
