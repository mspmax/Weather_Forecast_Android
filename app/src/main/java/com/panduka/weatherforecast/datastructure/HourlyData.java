package com.panduka.weatherforecast.datastructure;

/**
 * Created by Panduka on 2/13/2016.
 */
public class HourlyData {
    public long time;
    public String summary;
    public String icon;
    public double precipIntensity;
    public double precipProbability;
    public double temperature;
    public double apparentTemperature;
    public double dewPoint;
    public double humidity;
    public double windSpeed;
    public int windBearing;
    public double cloudCover;
    public double pressure;
    public double ozone;

}
