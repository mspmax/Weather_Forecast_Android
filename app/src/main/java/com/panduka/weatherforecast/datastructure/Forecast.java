package com.panduka.weatherforecast.datastructure;

/**
 * Created by Panduka on 2/13/2016.
 */
public class Forecast {
    public double latitude;
    public double longitude;
    public String timezone;
    public String offset;
    public HourlyData currently;
    public HourlyStruct hourly;
    public DailyStruct daily;
    public Flags flags;
    public Alerts alerts[];
}
