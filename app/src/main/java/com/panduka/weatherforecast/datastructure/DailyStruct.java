package com.panduka.weatherforecast.datastructure;

/**
 * Created by Panduka on 2/13/2016.
 */
public class DailyStruct {
    public String summary;
    public String icon;
    public DailyData[] data;

}
