package com.panduka.weatherforecast.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.panduka.weatherforecast.R;
import com.panduka.weatherforecast.datastructure.HourlyData;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Panduka on 2/13/2016.
 */
public class HourlyAdapter extends ArrayAdapter<HourlyData> {

    public HourlyAdapter(Context context, HourlyData[] objects) {
        super(context, R.layout.item_hourly_list, R.id.time, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // View holder design pattern is not used here, instead the convert view form the android system (super class) it self
        // is used, so it initializes everything and its not necessary to implement inflate logic manually
        View rootView = super.getView(position, convertView, parent);

        //setting up related data from the data structure to corresponding views
        ((TextView) rootView.findViewById(R.id.time)).setText(DateFormat.getTimeInstance().format(new Date(getItem(position).time * 1000)));
        ((TextView) rootView.findViewById(R.id.summary)).setText(getItem(position).summary);
        ((TextView) rootView.findViewById(R.id.precipIntensity)).setText("Precipitation Intensity : " + String.valueOf(getItem(position).precipIntensity));
        ((TextView) rootView.findViewById(R.id.precipProbability)).setText("Precipitation Probability : " + String.valueOf(getItem(position).precipProbability));
        ((TextView) rootView.findViewById(R.id.temperature)).setText(String.valueOf(getItem(position).temperature)+"\u2109");
        ((TextView) rootView.findViewById(R.id.apparentTemperature)).setText("Apparent Temperature : " + String.valueOf(getItem(position).apparentTemperature));
        ((TextView) rootView.findViewById(R.id.dewPoint)).setText("Dew Point : " + String.valueOf(getItem(position).dewPoint));
        ((TextView) rootView.findViewById(R.id.humidity)).setText("Humidity : " + String.valueOf(getItem(position).humidity));
        ((TextView) rootView.findViewById(R.id.windSpeed)).setText("Wind Speed : " + String.valueOf(getItem(position).windSpeed));
        ((TextView) rootView.findViewById(R.id.windBearing)).setText("Wind Bearing : " + String.valueOf(getItem(position).windBearing));
        ((TextView) rootView.findViewById(R.id.cloudCover)).setText("Cloud Cover : " + String.valueOf(getItem(position).cloudCover));
        ((TextView) rootView.findViewById(R.id.pressure)).setText("Pressure : " + String.valueOf(getItem(position).pressure));
        ((TextView) rootView.findViewById(R.id.ozone)).setText("Ozone : " + String.valueOf(getItem(position).ozone));

        if(getItem(position).icon.equalsIgnoreCase("clear-day")){
            ((ImageView) rootView.findViewById(R.id.weatherIcon)).setImageResource(R.drawable.sunny);
        }else if(getItem(position).icon.equalsIgnoreCase("clear-night")){
            ((ImageView) rootView.findViewById(R.id.weatherIcon)).setImageResource(R.drawable.moon);
        }else if(getItem(position).icon.equalsIgnoreCase("rain")){
            ((ImageView) rootView.findViewById(R.id.weatherIcon)).setImageResource(R.drawable.drizzle);
        }else if(getItem(position).icon.equalsIgnoreCase("snow")){
            ((ImageView) rootView.findViewById(R.id.weatherIcon)).setImageResource(R.drawable.snow);
        }else if(getItem(position).icon.equalsIgnoreCase("sleet")){
            ((ImageView) rootView.findViewById(R.id.weatherIcon)).setImageResource(R.drawable.drizzle_snow);
        }else if(getItem(position).icon.equalsIgnoreCase("wind")){
            ((ImageView) rootView.findViewById(R.id.weatherIcon)).setImageResource(R.drawable.haze);
        }else if(getItem(position).icon.equalsIgnoreCase("fog")){
            ((ImageView) rootView.findViewById(R.id.weatherIcon)).setImageResource(R.drawable.haze);
        }else if(getItem(position).icon.equalsIgnoreCase("cloudy")){
            ((ImageView) rootView.findViewById(R.id.weatherIcon)).setImageResource(R.drawable.cloudy);
        }else if(getItem(position).icon.equalsIgnoreCase("partly-cloudy-day")){
            ((ImageView) rootView.findViewById(R.id.weatherIcon)).setImageResource(R.drawable.mostly_cloudy);
        }else if(getItem(position).icon.equalsIgnoreCase("partly-cloudy-night")){
            ((ImageView) rootView.findViewById(R.id.weatherIcon)).setImageResource(R.drawable.cloudy_night);
        }else{
            ((ImageView) rootView.findViewById(R.id.weatherIcon)).setImageResource(R.drawable.slight_drizzle);
        }

        return rootView;
    }
}
