package com.panduka.weatherforecast.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.panduka.weatherforecast.R;
import com.panduka.weatherforecast.datastructure.DailyData;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Panduka on 2/13/2016.
 */
public class DailyAdapter extends ArrayAdapter<DailyData> {

    public DailyAdapter(Context context, DailyData[] objects) {
        super(context, R.layout.item_daily_list, R.id.time, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // View holder design pattern is not used here, instead the convert view form the android system (super class) it self
        // is used, so it initializes everything and its not necessary to implement inflate logic manually
        View rootView = super.getView(position, convertView, parent);

        //setting up related data from the data structure to corresponding views
        ((TextView) rootView.findViewById(R.id.time)).setText(DateFormat.getDateInstance().format(new Date(getItem(position).time * 1000)));
        ((TextView) rootView.findViewById(R.id.summary)).setText(getItem(position).summary);
        ((TextView) rootView.findViewById(R.id.icon)).setText("Icon : " + getItem(position).icon);
        ((TextView) rootView.findViewById(R.id.sunriseTime)).setText("Sunrise : " + SimpleDateFormat.getInstance().format(new Date(getItem(position).sunriseTime * 1000)));
        ((TextView) rootView.findViewById(R.id.sunsetTime)).setText("Sunset : " + SimpleDateFormat.getInstance().format(new Date(getItem(position).sunsetTime * 1000)));
        ((TextView) rootView.findViewById(R.id.moonPhase)).setText("Moon Phase : " + String.valueOf(getItem(position).moonPhase));
        ((TextView) rootView.findViewById(R.id.precipIntensity)).setText("Intensity : " + String.valueOf(getItem(position).precipIntensity));
        ((TextView) rootView.findViewById(R.id.precipIntensityMax)).setText("Intensity Max : " + String.valueOf(getItem(position).precipIntensityMax));
        ((TextView) rootView.findViewById(R.id.precipIntensityMaxTime)).setText("Intensity Max Time : " + SimpleDateFormat.getInstance().format(new Date(getItem(position).precipIntensityMaxTime * 1000)));
        ((TextView) rootView.findViewById(R.id.precipProbability)).setText("Probability : " + String.valueOf(getItem(position).precipProbability));
        ((TextView) rootView.findViewById(R.id.precipType)).setText("Type : " + String.valueOf(getItem(position).precipType));
        ((TextView) rootView.findViewById(R.id.temperatureMin)).setText("Min : " + String.valueOf(getItem(position).temperatureMin));
        ((TextView) rootView.findViewById(R.id.temperatureMinTime)).setText("Min Time : " + SimpleDateFormat.getInstance().format(new Date(getItem(position).temperatureMinTime * 1000)));
        ((TextView) rootView.findViewById(R.id.temperatureMax)).setText("Max : " + String.valueOf(getItem(position).temperatureMax));
        ((TextView) rootView.findViewById(R.id.temperatureMaxTime)).setText("Max Time : " + SimpleDateFormat.getInstance().format(new Date(getItem(position).temperatureMaxTime * 1000)));
        ((TextView) rootView.findViewById(R.id.apparentTemperatureMin)).setText("Min : " + String.valueOf(getItem(position).apparentTemperatureMin));
        ((TextView) rootView.findViewById(R.id.apparentTemperatureMinTime)).setText("Min Time : " + SimpleDateFormat.getInstance().format(new Date(getItem(position).apparentTemperatureMinTime * 1000)));
        ((TextView) rootView.findViewById(R.id.apparentTemperatureMax)).setText("Max : " + String.valueOf(getItem(position).apparentTemperatureMax));
        ((TextView) rootView.findViewById(R.id.apparentTemperatureMaxTime)).setText("Max Time : " + SimpleDateFormat.getInstance().format(new Date(getItem(position).apparentTemperatureMaxTime * 1000)));
        ((TextView) rootView.findViewById(R.id.dewPoint)).setText("Dew Point : " + String.valueOf(getItem(position).dewPoint));
        ((TextView) rootView.findViewById(R.id.humidity)).setText("Humidity : " + String.valueOf(getItem(position).humidity));
        ((TextView) rootView.findViewById(R.id.windSpeed)).setText("Wind Speed : " + String.valueOf(getItem(position).windSpeed));
        ((TextView) rootView.findViewById(R.id.windBearing)).setText("Wind Bearing : " + String.valueOf(getItem(position).windBearing));
        ((TextView) rootView.findViewById(R.id.cloudCover)).setText("Cloud Cover : " + String.valueOf(getItem(position).cloudCover));
        ((TextView) rootView.findViewById(R.id.pressure)).setText("Pressure : " + String.valueOf(getItem(position).pressure));
        ((TextView) rootView.findViewById(R.id.ozone)).setText("Ozone : " + String.valueOf(getItem(position).ozone));
        return rootView;
    }
}