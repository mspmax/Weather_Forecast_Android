package com.panduka.weatherforecast;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.widget.Toast;

import com.panduka.weatherforecast.listeners.GPSLocationListener;

/**
 * Created by Panduka on 2/13/2016.
 */
public class WeatherForecastApplication extends Application {

    public static final String W_FORECAST_API_KEY="2a1239d5b03a43031469b78b1edd41ef";
    public static final String W_FORECAST_URL="https://api.forecast.io/forecast/";

    @Override
    public void onCreate() {
        super.onCreate();
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        LocationListener mlocListener = new GPSLocationListener(this, W_FORECAST_URL, W_FORECAST_API_KEY);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(WeatherForecastApplication.this, "No User Permission.", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        //Application main invocation point
        //invokes onLocationChanged callback in GPSLocationListener and eventually it will invoke the Intent Service every
        //1 Km location change (1000 represents distance change in meters)
        //and which will start to download data from the web service.
        //0 = time irrelevant
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 1000, mlocListener);
    }
}
